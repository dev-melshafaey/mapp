<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'customer-list',
            'customer-create',
            'customer-edit',
            'customer-delete',
            'product-list',
            'product-create',
            'product-edit',
            'product-delete',
            'pay-list',
            'pay-create',
            'pay-edit',
            'pay-delete',
            'quot-list',
            'quot-create',
            'quot-edit',
            'quot-delete',
            'inv-list',
            'inv-create',
            'inv-edit',
            'inv-delete',
            'gall-list',
            'gall-create',
            'gall-edit',
            'gall-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
        ];


        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);
        }
    }

}
