<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuotationItemsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('quotation_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('item_id');
            $table->integer('quotation_id');
            $table->text('description');
            $table->double('qty');
            $table->double('unit_price');
            $table->double('discount');
            $table->double('tax');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('quotation_items');
    }

}
