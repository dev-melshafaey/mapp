/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.28-MariaDB : Database - mapp
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `attends` */

DROP TABLE IF EXISTS `attends`;

CREATE TABLE `attends` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `state` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `attends` */

insert  into `attends`(`id`,`user_id`,`state`,`created_at`,`updated_at`) values (1,1,'in','2018-05-15 13:55:35','2018-05-15 13:55:35'),(2,1,'out','2018-05-30 14:06:24','2018-05-30 14:06:24'),(3,1,'in','2018-05-30 14:06:55','2018-05-30 14:06:55'),(4,1,'out','2018-05-30 14:07:11','2018-05-30 14:07:11'),(5,1,'in','2018-05-30 14:07:13','2018-05-30 14:07:13'),(6,1,'out','2018-05-30 14:07:15','2018-05-30 14:07:15'),(7,1,'in','2018-05-30 14:07:16','2018-05-30 14:07:16'),(8,1,'out','2018-05-30 14:07:17','2018-05-30 14:07:17'),(9,1,'in','2018-05-30 14:07:19','2018-05-30 14:07:19'),(10,1,'out','2018-05-30 14:33:10','2018-05-30 14:33:10'),(11,1,'in','2018-05-30 14:36:35','2018-05-30 14:36:35'),(12,1,'out','2018-05-30 14:36:36','2018-05-30 14:36:36'),(13,3,'in','2018-05-29 11:01:24','2018-05-31 11:01:24'),(14,3,'out','2018-05-29 11:02:23','2018-05-31 11:02:23'),(15,3,'in','2018-05-31 11:02:32','2018-05-31 11:02:32'),(16,3,'out','2018-05-31 11:04:21','2018-05-31 11:04:21'),(20,1,'in','2018-05-31 11:35:28','2018-05-31 11:35:28');

/*Table structure for table `customers` */

DROP TABLE IF EXISTS `customers`;

CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customers_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `customers` */

insert  into `customers`(`id`,`name`,`email`,`phone`,`address`,`website`,`logo`,`created_at`,`updated_at`) values (1,'wood and design','info@woodanddesign.com','011','Cairo','www.woodanddesign.net','uploads/logos/1525708728.png','2018-05-07 15:58:48','2018-05-07 15:58:48'),(16,'twins','info@twins.com','010','Cairo','www.twins.com','','2018-05-07 16:32:45','2018-05-08 09:57:20');

/*Table structure for table `galleries` */

DROP TABLE IF EXISTS `galleries`;

CREATE TABLE `galleries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=43 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `galleries` */

insert  into `galleries`(`id`,`image`,`tag`,`user_id`,`created_at`,`updated_at`) values (3,'1527502809.png','test',1,'2018-05-28 10:20:09','2018-05-28 10:20:09'),(4,'1527502809.jpg','test',1,'2018-05-28 10:20:09','2018-05-28 10:20:09'),(5,'1527502810.jpg','test',1,'2018-05-28 10:20:12','2018-05-28 10:20:12'),(6,'1527502812.png','test',1,'2018-05-28 10:20:12','2018-05-28 10:20:12'),(7,'1527502814.jpg','test',1,'2018-05-28 10:20:14','2018-05-28 10:20:14'),(8,'1527502828.png','dd',1,'2018-05-28 10:20:30','2018-05-28 10:20:30'),(9,'1527502842.jpg','uio',1,'2018-05-28 10:20:45','2018-05-28 10:20:45'),(10,'1527502849.jpg','test',1,'2018-05-28 10:20:51','2018-05-28 10:20:51'),(11,'1527502851.jpg','test',1,'2018-05-28 10:20:53','2018-05-28 10:20:53'),(12,'1527502855.jpg','test',1,'2018-05-28 10:20:56','2018-05-28 10:20:56'),(13,'1527502858.jpg','test',1,'2018-05-28 10:21:00','2018-05-28 10:21:00'),(14,'1527502882.jpg','test',1,'2018-05-28 10:21:23','2018-05-28 10:21:23'),(15,'1527502887.jpg','test',1,'2018-05-28 10:21:29','2018-05-28 10:21:29'),(16,'1527502891.jpg','test',1,'2018-05-28 10:21:33','2018-05-28 10:21:33'),(17,'1527502894.jpg','test',1,'2018-05-28 10:21:35','2018-05-28 10:21:35'),(18,'1527502897.jpg','test',1,'2018-05-28 10:21:40','2018-05-28 10:21:40'),(19,'1527502903.jpg','test',1,'2018-05-28 10:21:44','2018-05-28 10:21:44'),(20,'1527502906.jpg','test',1,'2018-05-28 10:21:47','2018-05-28 10:21:47'),(21,'1527502908.jpg','test',1,'2018-05-28 10:21:50','2018-05-28 10:21:50'),(22,'1527502912.jpg','test',1,'2018-05-28 10:21:53','2018-05-28 10:21:53'),(23,'1527502917.jpg','test',1,'2018-05-28 10:21:58','2018-05-28 10:21:58'),(24,'1527502932.jpg','test',1,'2018-05-28 10:22:15','2018-05-28 10:22:15'),(25,'1527502938.jpg','test',1,'2018-05-28 10:22:22','2018-05-28 10:22:22'),(26,'1527502948.jpg','test',1,'2018-05-28 10:22:31','2018-05-28 10:22:31'),(27,'1527502957.jpg','test',1,'2018-05-28 10:22:41','2018-05-28 10:22:41'),(28,'1527502963.jpg','test',1,'2018-05-28 10:22:45','2018-05-28 10:22:45'),(29,'1527502967.jpg','test',1,'2018-05-28 10:22:49','2018-05-28 10:22:49'),(30,'1527502970.jpg','test',1,'2018-05-28 10:22:51','2018-05-28 10:22:51'),(31,'1527502972.jpg','test',1,'2018-05-28 10:22:53','2018-05-28 10:22:53'),(32,'1527502973.jpg','test',1,'2018-05-28 10:22:53','2018-05-28 10:22:53'),(33,'1527502974.jpg','test',1,'2018-05-28 10:22:54','2018-05-28 10:22:54'),(34,'1527502975.jpg','test',1,'2018-05-28 10:22:55','2018-05-28 10:22:55'),(35,'1527502976.jpg','test',1,'2018-05-28 10:22:56','2018-05-28 10:22:56'),(36,'1527502976.jpg','test',1,'2018-05-28 10:22:56','2018-05-28 10:22:56'),(37,'1527502977.jpg','test',1,'2018-05-28 10:22:57','2018-05-28 10:22:57'),(38,'1527502978.jpg','test',1,'2018-05-28 10:22:58','2018-05-28 10:22:58'),(39,'1527502978.jpg','test',1,'2018-05-28 10:22:58','2018-05-28 10:22:58'),(40,'1527502979.jpg','test',1,'2018-05-28 10:22:59','2018-05-28 10:22:59'),(41,'1527502980.jpg','test',1,'2018-05-28 10:23:00','2018-05-28 10:23:00'),(42,'1527502980.jpg','test',1,'2018-05-28 10:23:03','2018-05-28 10:23:03');

/*Table structure for table `invoice_items` */

DROP TABLE IF EXISTS `invoice_items`;

CREATE TABLE `invoice_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `qty` double NOT NULL,
  `unit_price` double NOT NULL,
  `discount` double NOT NULL,
  `tax` double NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `invoice_items` */

insert  into `invoice_items`(`id`,`item_id`,`invoice_id`,`description`,`qty`,`unit_price`,`discount`,`tax`,`created_at`,`updated_at`) values (3,3,2,NULL,1,10,0,0,'2018-05-13 16:57:12','2018-05-13 16:57:12'),(5,4,4,NULL,11,11,0,0,'2018-05-15 11:03:33','2018-05-15 11:03:33'),(6,4,3,NULL,3,10,0,0,'2018-05-15 11:17:57','2018-05-15 11:17:57'),(7,4,3,NULL,4,20,0,0,'2018-05-15 11:17:57','2018-05-15 11:17:57'),(10,3,6,NULL,1,13,0,10,'2018-05-15 15:03:35','2018-05-15 15:03:35');

/*Table structure for table `invoice_payments` */

DROP TABLE IF EXISTS `invoice_payments`;

CREATE TABLE `invoice_payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `invoice_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `payment_date` date NOT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `invoice_payments` */

insert  into `invoice_payments`(`id`,`invoice_id`,`amount`,`payment_date`,`file`,`notes`,`created_at`,`updated_at`) values (1,4,1000,'2018-05-15','uploads/documents/1526391785.pdf','payment notes','2018-05-15 13:43:05','2018-05-15 13:43:05'),(2,4,1,'2018-05-15',NULL,NULL,'2018-05-15 13:46:03','2018-05-15 13:46:03'),(3,3,10,'2018-05-15',NULL,'payment 1','2018-05-15 14:39:39','2018-05-15 14:39:39'),(4,3,100,'2018-05-15',NULL,NULL,'2018-05-15 14:47:35','2018-05-15 14:47:35'),(5,6,14.3,'2018-05-15',NULL,NULL,'2018-05-15 15:14:00','2018-05-15 15:14:00');

/*Table structure for table `invoices` */

DROP TABLE IF EXISTS `invoices`;

CREATE TABLE `invoices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `invoice_date` date NOT NULL,
  `expire_date` date DEFAULT NULL,
  `invoice_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_value` double NOT NULL,
  `payment_id` int(11) NOT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `paid` bit(1) DEFAULT b'0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `invoices` */

insert  into `invoices`(`id`,`title`,`customer_id`,`invoice_date`,`expire_date`,`invoice_address`,`delivery_address`,`total_value`,`payment_id`,`notes`,`file`,`status`,`created_at`,`updated_at`,`paid`) values (2,'INV887743',1,'2018-05-13',NULL,'info@woodanddesign.com','info@woodanddesign.com',2,2,NULL,'pdf/invoices/INV887743_1526230625.pdf','Approved','2018-05-13 16:57:12','2018-05-13 16:57:12','\0'),(3,'INV299582',16,'2018-05-15','2018-05-31','info@twins.com','info@twins.com',110,2,'testy','pdf/invoices/INV299582_1526383070.pdf','Approved','2018-05-15 11:02:19','2018-05-15 14:47:35',''),(4,'INV183311',1,'2018-05-15',NULL,'info@woodanddesign.com','info@woodanddesign.com',2,2,NULL,'pdf/invoices/INV183311_1526382206.pdf','Approved','2018-05-15 11:03:33','2018-05-15 13:46:03',''),(6,'INV9050',1,'2018-05-15',NULL,'info@woodanddesign.com','info@woodanddesign.com',14.3,2,NULL,'pdf/invoices/INV9050_1526396610.pdf','Approved','2018-05-15 15:03:35','2018-05-15 15:14:00','');

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2018_05_07_124421_create_permission_tables',1),(4,'2018_05_07_141042_create_customers_table',2),(5,'2018_05_08_113615_create_quotations_table',3),(6,'2018_05_08_114456_create_quotation_items_table',4),(7,'2018_05_08_115504_create_product_items_table',5),(8,'2018_05_08_132021_create_payments_table',6),(9,'2018_05_13_150242_create_invoices_table',7),(10,'2018_05_13_150501_create_invoice_items_table',7),(11,'2018_05_15_131417_create_invoice_payments_table',8),(12,'2018_05_15_151627_create_galleries_table',9),(13,'2018_05_28_113312_create_attends_table',10);

/*Table structure for table `model_has_permissions` */

DROP TABLE IF EXISTS `model_has_permissions`;

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `model_has_permissions` */

/*Table structure for table `model_has_roles` */

DROP TABLE IF EXISTS `model_has_roles`;

CREATE TABLE `model_has_roles` (
  `role_id` int(10) unsigned NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) unsigned NOT NULL,
  PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`),
  CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `model_has_roles` */

insert  into `model_has_roles`(`role_id`,`model_type`,`model_id`) values (2,'App\\User',1),(3,'App\\User',3);

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `payments` */

DROP TABLE IF EXISTS `payments`;

CREATE TABLE `payments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `payments` */

insert  into `payments`(`id`,`name`,`created_at`,`updated_at`) values (2,'payment method 1','2018-05-08 13:31:01','2018-05-08 13:31:01');

/*Table structure for table `permissions` */

DROP TABLE IF EXISTS `permissions`;

CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `permissions` */

insert  into `permissions`(`id`,`name`,`guard_name`,`created_at`,`updated_at`) values (1,'role-list','web','2018-05-30 11:00:24','2018-05-30 11:00:24'),(2,'role-create','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(3,'role-edit','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(4,'role-delete','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(5,'customer-list','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(6,'customer-create','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(7,'customer-edit','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(8,'customer-delete','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(9,'product-list','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(10,'product-create','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(11,'product-edit','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(12,'product-delete','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(14,'payement-list','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(15,'payement-create','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(16,'payement-edit','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(17,'payement-delete','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(18,'quotation-list','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(19,'quotation-create','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(20,'quotation-edit','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(21,'quotation-delete','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(22,'invoice-list','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(23,'invoice-create','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(24,'invoice-edit','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(25,'invoice-delete','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(26,'gallery-list','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(27,'gallery-create','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(28,'gallery-edit','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(29,'gallery-delete','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(30,'user-list','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(31,'user-create','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(32,'user-edit','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(33,'user-delete','web','2018-05-30 11:00:25','2018-05-30 11:00:25'),(34,'sign-in/sign-out','web','2018-05-30 16:22:00','2018-05-30 16:22:02');

/*Table structure for table `product_items` */

DROP TABLE IF EXISTS `product_items`;

CREATE TABLE `product_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `unit_price` double NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `product_items` */

insert  into `product_items`(`id`,`name`,`description`,`unit_price`,`created_at`,`updated_at`) values (3,'item3',NULL,1,'2018-05-08 12:30:05','2018-05-08 12:30:05'),(4,'item1',NULL,1,NULL,NULL),(5,'item2',NULL,1,NULL,NULL),(6,'item4',NULL,1,NULL,NULL);

/*Table structure for table `quotation_items` */

DROP TABLE IF EXISTS `quotation_items`;

CREATE TABLE `quotation_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `item_id` int(11) NOT NULL,
  `quotation_id` int(11) NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `qty` double NOT NULL DEFAULT '1',
  `unit_price` double NOT NULL DEFAULT '1',
  `discount` double NOT NULL DEFAULT '0',
  `tax` double NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `quotation_items` */

insert  into `quotation_items`(`id`,`item_id`,`quotation_id`,`description`,`qty`,`unit_price`,`discount`,`tax`,`created_at`,`updated_at`) values (5,3,5,NULL,1,10,0,0,'2018-05-13 09:56:33','2018-05-13 09:56:33'),(12,6,6,NULL,3,10,0,0,'2018-05-13 11:09:38','2018-05-13 11:09:38'),(13,4,6,NULL,2,10,0,0,'2018-05-13 11:09:38','2018-05-13 11:09:38'),(14,5,6,NULL,1,13,0,0,'2018-05-13 11:09:38','2018-05-13 11:09:38');

/*Table structure for table `quotations` */

DROP TABLE IF EXISTS `quotations`;

CREATE TABLE `quotations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_id` int(11) NOT NULL,
  `quotation_date` date NOT NULL,
  `expire_date` date DEFAULT NULL,
  `invoice_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payment_id` int(11) NOT NULL,
  `total_value` double DEFAULT NULL,
  `file` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `notes` text COLLATE utf8mb4_unicode_ci,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT 'Pending',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `quotations` */

insert  into `quotations`(`id`,`title`,`customer_id`,`quotation_date`,`expire_date`,`invoice_address`,`delivery_address`,`payment_id`,`total_value`,`file`,`notes`,`status`,`created_at`,`updated_at`) values (5,'QU764846',1,'2018-05-13',NULL,'info@woodanddesign.com','info@woodanddesign.com',2,2,'pdf/quotations/QU764846_1526205382.pdf',NULL,'Approved','2018-05-13 09:56:33','2018-05-13 13:49:47'),(6,'QU764846',1,'2018-05-13',NULL,'info@woodanddesign.com','info@woodanddesign.com',2,2,'pdf/quotations/QU764846_1526209769.pdf','test','Approved','2018-05-13 09:58:04','2018-05-13 13:10:07');

/*Table structure for table `role_has_permissions` */

DROP TABLE IF EXISTS `role_has_permissions`;

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `role_has_permissions_role_id_foreign` (`role_id`),
  CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `role_has_permissions` */

insert  into `role_has_permissions`(`permission_id`,`role_id`) values (1,2),(2,2),(3,2),(4,2),(5,2),(6,2),(7,2),(8,2),(9,2),(10,2),(11,2),(12,2),(14,2),(15,2),(16,2),(17,2),(18,2),(19,2),(20,2),(21,2),(22,2),(23,2),(24,2),(25,2),(26,2),(27,2),(28,2),(29,2),(30,2),(31,2),(32,2),(33,2),(34,2),(34,3);

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`name`,`guard_name`,`created_at`,`updated_at`) values (2,'Admin','web','2018-05-30 12:25:26','2018-05-30 12:25:26'),(3,'employee','web','2018-05-30 14:22:20','2018-05-30 14:22:20');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`name`,`email`,`password`,`remember_token`,`created_at`,`updated_at`) values (1,'khaled','khaled@marketingmisr.com','$2y$10$hA4svNYP3QhT.Wnrl20WOOeDqTWF3iV4oKrulAanlpoABxePIg0Y.','KwkBt9CaeZrMH9n2xMGya5kfef1eJEzxL02s4AhbNQr2DHzfnLEjP1RDhbqc','2018-05-07 13:36:19','2018-05-31 10:59:07'),(3,'Mahmoud Elshafaey','mm_shifo@yahoo.com','$2y$10$EW4rKxoC9ETEXsmg1KKnR.EpkyL/UxT9BgEKpm4sInxWaj5AOnD/q','iJW1Oq6LeRNBI0awSGWex9Vb6eZDydWI2MuLs4Pw4uCabQCzuQ1cg6glZv2v','2018-05-31 11:00:33','2018-05-31 11:00:33');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
