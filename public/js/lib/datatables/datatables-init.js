$(document).ready(function () {
    $('#myTable').DataTable();
    $(document).ready(function () {
        var table = $('#example').DataTable({
            "columnDefs": [{
                    "visible": false,
                    "targets": 2
                }],

            "displayLength": 25,
            "drawCallback": function (settings) {
                var api = this.api();
                var rows = api.rows({
                    page: 'current'
                }).nodes();
                var last = null;
                api.column(2, {
                    page: 'current'
                }).data().each(function (group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before('<tr class="group"><td colspan="5">' + group + '</td></tr>');
                        last = group;
                    }
                });
            }
        });
        // Order by the grouping
        $('#example tbody').on('click', 'tr.group', function () {
            var currentOrder = table.order()[0];
            if (currentOrder[0] === 2 && currentOrder[1] === 'asc') {
                table.order([2, 'desc']).draw();
            } else {
                table.order([2, 'asc']).draw();
            }
        });
    });
});
$('#example23').DataTable({
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
   
    initComplete: function () {

        this.api().columns().every(function () {
            var column = this;
            var select = $('<select class="form-control" style="width:auto"><option value="">Search</option></select>')
                    .appendTo($(column.footer()).empty())
                    .on('change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                                $(this).val()
                                );

                        column
                                .search(val ? '^' + val + '$' : '', true, false)
                                .draw();
                    });

            column.data().unique().sort().each(function (d, j) {
                if (d !== '') {
                    select.append('<option value="' + d + '">' + d + '</option>')
                }
            });
        });
        var r = $('#example23 tfoot tr');
        r.find('th').each(function () {
            $(this).css('padding', 8);
        });
        $('#example23 thead').append(r);
        $('#search_0').css('text-align', 'center');
    }
});
