<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Quotation extends Model {
    
    public function customer() {
        return $this->belongsTo('App\Customer');
    }
    
     public function payment() {
        return $this->belongsTo('App\Payment');
    }

}
