<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuotationItem extends Model {

    public function quotation() {
        return $this->belongsTo('App\Quotation', 'id', 'quotation_id');
    }

    public function item() {
        return $this->belongsTo('App\ProductItem');
    }

}
