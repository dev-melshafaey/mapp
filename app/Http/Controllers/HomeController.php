<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class HomeController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $state = \App\attends::where('user_id', auth()->id())->orderBy('created_at', 'desc')->first();

        $isAdmin = \App\User::whereHas('roles', function($q) {
                    $q->where('name', 'Admin');
                })->where('id', auth()->id())->first();

        if (!empty($isAdmin)) {
            $attends = \App\attends::whereDate('created_at', \DB::raw('CURDATE()'))->orderBy('created_at', 'desc')->get();
        } else {
            $attends = \App\attends::whereDate('created_at', \DB::raw('CURDATE()'))->where([['user_id', auth()->id()]])->orderBy('created_at', 'desc')->get();
        }

        if (empty($state)) {
            $state = 'out';
        } else {
            $state = $state->state;
        }

        return view('layouts.dashboard', compact('state', 'attends'));
    }

}
