<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use Illuminate\Support\Facades\Mail;

class InvoiceController extends Controller {

    function __construct() {
        $this->middleware('permission:invoice-list');
        $this->middleware('permission:invoice-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:invoice-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:invoice-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $invoices = Invoice::orderBy('created_at', 'desc')->get();
        return view('invoice.show', ['invoices' => $invoices]);
    }

    public function generateRandomString($randomString, $length = 6) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $customers = \App\Customer::all();
        $payments = \App\Payment::all();
        $title = $this->generateRandomString('INV', 4);
        return view('invoice.add-edit', ['customers' => $customers, 'payments' => $payments, 'title' => $title]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'title' => 'required',
            'customer_id' => 'required|not_in:0',
            'order_date' => 'required',
            'invoice_address' => 'required',
            'delivery_address' => 'required',
        ]);

        $invoice = new Invoice();

        $invoice->title = $request->title;
        $invoice->customer_id = $request->customer_id;
        $invoice->invoice_date = $request->order_date;
        $invoice->expire_date = $request->expiry_date;
        $invoice->invoice_address = $request->invoice_address;
        $invoice->delivery_address = $request->delivery_address;
        $invoice->notes = $request->notes;
        $invoice->payment_id = $request->payment_id;
        $invoice->total_value = str_replace(',', '', $request->grand_total);

        $pdf = \PDF::loadView('pdf.invoice', compact('request'));
        $filname = $request->title . '_' . time() . '.pdf';
        \Storage::put('public/pdf/invoices/' . $filname, $pdf->output());
        $invoice->file = 'pdf/invoices/' . $filname;

        $invoice->save();

        if ($request->items) {
            foreach ($request->items as $i => $value) {
                $item = new \App\InvoiceItem();
                $item->item_id = explode('_', $value)[0];
                $item->invoice_id = $invoice->id;
                $item->description = $request->description[$i];
                $item->qty = $request->qtys[$i];
                $item->unit_price = $request->unit_prices[$i];
                $item->discount = $request->discounts[$i];
                $item->tax = $request->taxes[$i];

                $item->save();
            }
            if ($request->submit === 'save') {
                return redirect('invoice')->with('status', 'Added Successfuly');
            } else {
                $invoice = Invoice::find($invoice->id);
                $invoice->status = 'Approved';
                $invoice->update();

                $pdf->download($filname);
                $customer = \App\Customer::find($invoice->customer_id);
                $text = 'Hello ' . $customer->name . ',<br/>Kindly find the attached invoice.<br/>Please let me know if you have any concerns.<br/>Regards,<br/>Marketing Misr';
                return view('invoice.compose', compact('invoice', 'customer', 'text'));
            }
        } else {
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $customers = \App\Customer::all();
        $payments = \App\Payment::all();
        $invoice = Invoice::find($id);
        $invoice_items = \App\InvoiceItem::where('invoice_id', $id)->get();
        $prod_items = \App\ProductItem::all();
        return view('invoice.add-edit', [
            'customers' => $customers,
            'payments' => $payments,
            'invoice' => $invoice,
            'invoice_items' => $invoice_items,
            'prod_items' => $prod_items
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {

        $this->validate($request, [
            'title' => 'required',
            'customer_id' => 'required|not_in:0',
            'order_date' => 'required',
            'invoice_address' => 'required',
            'delivery_address' => 'required',
        ]);

        $invoice = Invoice::find($id);

        $invoice->title = $request->title;
        $invoice->customer_id = $request->customer_id;
        $invoice->invoice_date = $request->order_date;
        $invoice->expire_date = $request->expiry_date;
        $invoice->invoice_address = $request->invoice_address;
        $invoice->delivery_address = $request->delivery_address;
        $invoice->notes = $request->notes;
        $invoice->payment_id = $request->payment_id;
        $invoice->total_value = str_replace(',', '', $request->grand_total);

        $pdf = \PDF::loadView('pdf.invoice', compact('request'));
        $filname = $request->title . '_' . time() . '.pdf';
        \Storage::put('public/pdf/invoices/' . $filname, $pdf->output());
        $invoice->file = 'pdf/invoices/' . $filname;

        $invoice->update();
        \App\InvoiceItem::where('invoice_id', $id)->delete();

        if ($request->items) {
            foreach ($request->items as $i => $value) {
                $item = new \App\InvoiceItem();
                $item->item_id = explode('_', $value)[0];
                $item->invoice_id = $invoice->id;
                $item->description = $request->description[$i];
                $item->qty = $request->qtys[$i];
                $item->unit_price = $request->unit_prices[$i];
                $item->discount = $request->discounts[$i];
                $item->tax = $request->taxes[$i];

                $item->save();
            }
            if ($request->submit === 'save') {
                return redirect('invoice')->with('status', 'Added Successfuly');
            } else {
                $invoice = Invoice::find($invoice->id);
                $invoice->status = 'Approved';
                $invoice->update();

                $pdf->download($filname);
                $customer = \App\Customer::find($invoice->customer_id);
                $text = 'Hello ' . $customer->name . ',<br/>Kindly find the attached invoice.<br/>Please let me know if you have any concerns.<br/>Regards,<br/>Marketing Misr';
                return view('invoice.compose', compact('invoice', 'customer', 'text'));
            }
        } else {
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $invoice = Invoice::findOrFail($id);
        $invoice->delete();

        \App\InvoiceItem::where('invoice_id', $id)->delete();
        return json_encode(TRUE);
    }

    public function send(Request $request) {
        $file = Invoice::find($request->invoice_id)->file;
        $request->message = str_replace('<br>', PHP_EOL, $request->message);
        Mail::send([], [], function ($message) use ($request, $file) {
            $message->to($request->to)->from('info@marketingmisr.com')
                    ->subject($request->subject)->attach('storage/' . $file, [
                        'as' => $file,
                        'mime' => 'pdf',
                    ])
                    ->setBody($request->message, 'text/html');
        });
        return redirect('invoice')->with('status', 'Added And Sent Successfuly');
    }

    public function status(Request $request) {
        $invoice = Invoice::find($request->id);
        $invoice->status = 'Approved';
        $invoice->update();
        return json_encode(TRUE);
    }

    public function pay($invoice_id) {
        $payments = \App\InvoicePayment::where('invoice_id', $invoice_id)->get();
        $invoice = Invoice::find($invoice_id);
        return view('invoice.pay', compact('payments', 'invoice_id', 'invoice'));
    }

    public function add_pay(Request $request, $id) {

        $this->validate($request, [
            'amount' => 'required|numeric|min:1',
            'date' => 'required',
            'document' => 'file|mimes:png,jpg,jpeg,bmp,pdf,doc,docx|max:2048',
        ]);

        $payment = new \App\InvoicePayment();
        $payment->invoice_id = $id;
        $payment->amount = $request->amount;
        $payment->payment_date = $request->date;
        $payment->notes = $request->notes;

        if ($request->document) {
            $document = $request->file('document');
            $file = time() . '.' . $document->getClientOriginalExtension();
            $document->storeAs('public/uploads/documents', $file);
            $payment->file = 'uploads/documents/' . $file;
        }

        if ($payment->save()) {
            if ($request->done == 'on') {
                $invoice = Invoice::find($id);
                $invoice->paid = 1;
                $invoice->update();
            }
            return redirect('invoice')->with('status', 'Added Successfuly');
        } else {
            return back()->withInput();
        }
    }

}
