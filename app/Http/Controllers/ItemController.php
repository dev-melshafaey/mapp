<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\ProductItem;

class ItemController extends Controller {

    function __construct() {
        $this->middleware('permission:product-list');
        $this->middleware('permission:product-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:product-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:product-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $items = ProductItem::orderBy('created_at', 'desc')->get();
        return view('item.show', ['items' => $items]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('item.add-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $item = new ProductItem();

        $item->name = $request->name;
        $item->unit_price = $request->unit_price;
        $item->description = $request->description;

        if ($item->save()) {
            return redirect('item')->with('status', 'Added Successfuly');
        } else {
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $items = ProductItem::all();
        echo json_encode($items);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $item = ProductItem::findOrFail($id);
        return view('item.add-edit', ['item' => $item]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $item = ProductItem::find($id);

        $item->name = $request->name;
        $item->unit_price = $request->unit_price;
        $item->description = $request->description;

        if ($item->update()) {
            return redirect('item')->with('status', 'Updated Successfuly');
        } else {
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $item = ProductItem::findOrFail($id);
        if ($item->delete()) {
            echo TRUE;
        } else {
            echo FALSE;
        }
    }

}
