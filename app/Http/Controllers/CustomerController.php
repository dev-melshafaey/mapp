<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller {

    function __construct() {
        $this->middleware('permission:customer-list');
        $this->middleware('permission:customer-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:customer-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:customer-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $customers = Customer::all();
        return view('customer.show', [
            'customers' => $customers
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('customer.add-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {

        $this->validate($request, [
            'name' => 'required',
            'email' => 'email|required',
            'phone' => 'required',
            'address' => 'required',
            'logo' => 'file|mimes:png,jpg,jpeg,bmp|max:2048',
        ]);

        $customer = new Customer();

        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->address = $request->address;
        $customer->website = $request->website;
        if ($request->logo) {
            $logo = $request->file('logo');
            $file = time() . '.' . $logo->getClientOriginalExtension();
            $logo->storeAs('public/uploads/logos', $file);
            $customer->logo = 'uploads/logos/' . $file;
        }

        if ($customer->save()) {
            return redirect('customer')->with('status', 'Added Successfuly');
        } else {
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $customer = Customer::findOrFail($id);
        echo json_encode($customer);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $customer = Customer::findOrFail($id);
        return view('customer.add-edit', ['customer' => $customer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'email|required',
            'phone' => 'required',
            'address' => 'required',
            'logo' => 'file|mimes:png,jpg,jpeg,bmp|max:2048',
        ]);

        $customer = Customer::find($id);

        $customer->name = $request->name;
        $customer->email = $request->email;
        $customer->phone = $request->phone;
        $customer->address = $request->address;
        $customer->website = $request->website;
        if ($request->logo) {
            $logo = $request->file('logo');
            $file = time() . '.' . $logo->getClientOriginalExtension();
            $logo->storeAs('public/uploads/logos', $file);
            $customer->logo = 'uploads/logos/' . $file;
        }

        if ($customer->update()) {
            return redirect('customer')->with('status', 'Updated Successfuly');
        } else {
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $customer = Customer::find($id);
        if ($customer->delete()) {
            echo TRUE;
        } else {
            echo FALSE;
        }
    }

}
