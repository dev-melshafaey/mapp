<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quotation;
use Illuminate\Support\Facades\Mail;

class QuotationController extends Controller {

    function __construct() {
        $this->middleware('permission:quotation-list');
        $this->middleware('permission:quotation-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:quotation-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:quotation-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $quotations = Quotation::orderBy('created_at', 'desc')->get();
        return view('quotation.show', ['quotations' => $quotations]);
    }

    public function generateRandomString($randomString, $length = 6) {
        $characters = '0123456789';
        $charactersLength = strlen($characters);
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $customers = \App\Customer::all();
        $payments = \App\Payment::all();
        $title = $this->generateRandomString('QU');
        return view('quotation.add-edit', ['customers' => $customers, 'payments' => $payments, 'title' => $title]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'title' => 'required',
            'customer_id' => 'required|not_in:0',
            'order_date' => 'required',
            'invoice_address' => 'required',
            'delivery_address' => 'required',
        ]);

        $quotation = new Quotation();

        $quotation->title = $request->title;
        $quotation->customer_id = $request->customer_id;
        $quotation->quotation_date = $request->order_date;
        $quotation->expire_date = $request->expiry_date;
        $quotation->invoice_address = $request->invoice_address;
        $quotation->delivery_address = $request->delivery_address;
        $quotation->notes = $request->notes;
        $quotation->payment_id = $request->payment_id;
        $quotation->total_value = str_replace(',', '', $request->grand_total);

        $pdf = \PDF::loadView('pdf.quotation', compact('request'));
        $filname = $request->title . '_' . time() . '.pdf';
        \Storage::put('public/pdf/quotations/' . $filname, $pdf->output());
        $quotation->file = 'pdf/quotations/' . $filname;

        $quotation->save();

        if ($request->items) {
            foreach ($request->items as $i => $value) {
                $item = new \App\QuotationItem();
                $item->item_id = explode('_', $value)[0];
                $item->quotation_id = $quotation->id;
                $item->description = $request->description[$i];
                $item->qty = $request->qtys[$i];
                $item->unit_price = $request->unit_prices[$i];
                $item->discount = $request->discounts[$i];
                $item->tax = $request->taxes[$i];

                $item->save();
            }
            if ($request->submit === 'save') {
                return redirect('quotation')->with('status', 'Added Successfuly');
            } else {
                $pdf->download($filname);
                $customer = \App\Customer::find($quotation->customer_id);
                $text = 'Hello ' . $customer->name . ',<br/>Kindly find the attached quotation.<br/>Please let me know if you have any concerns.<br/>Regards,<br/>Marketing Misr';
                return view('quotation.compose', compact('quotation', 'customer', 'text'));
            }
        } else {
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $customers = \App\Customer::all();
        $payments = \App\Payment::all();
        $quotation = Quotation::find($id);
        $quote_items = \App\QuotationItem::where('quotation_id', $id)->get();
        $prod_items = \App\ProductItem::all();
        return view('quotation.add-edit', [
            'customers' => $customers,
            'payments' => $payments,
            'quotation' => $quotation,
            'quote_items' => $quote_items,
            'prod_items' => $prod_items
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'title' => 'required',
            'customer_id' => 'required|not_in:0',
            'order_date' => 'required',
            'invoice_address' => 'required',
            'delivery_address' => 'required',
        ]);

        $quotation = Quotation::find($id);

        $quotation->title = $request->title;
        $quotation->customer_id = $request->customer_id;
        $quotation->quotation_date = $request->order_date;
        $quotation->expire_date = $request->expiry_date;
        $quotation->invoice_address = $request->invoice_address;
        $quotation->delivery_address = $request->delivery_address;
        $quotation->notes = $request->notes;
        $quotation->payment_id = $request->payment_id;
        $quotation->total_value = str_replace(',', '', $request->grand_total);

        $pdf = \PDF::loadView('pdf.quotation', compact('request'));
        $filname = $request->title . '_' . time() . '.pdf';
        \Storage::put('public/pdf/quotations/' . $filname, $pdf->output());
        $quotation->file = 'pdf/quotations/' . $filname;

        $quotation->update();
        \App\QuotationItem::where('quotation_id', $id)->delete();

        if ($request->items) {
            foreach ($request->items as $i => $value) {
                $item = new \App\QuotationItem();
                $item->item_id = explode('_', $value)[0];
                $item->quotation_id = $id;
                $item->description = $request->description[$i];
                $item->qty = $request->qtys[$i];
                $item->unit_price = $request->unit_prices[$i];
                $item->discount = $request->discounts[$i];
                $item->tax = $request->taxes[$i];

                $item->save();
            }
            if ($request->submit === 'save') {
                return redirect('quotation')->with('status', 'Updated Successfuly');
            } else {
                $pdf->download($filname);
                $customer = \App\Customer::find($quotation->customer_id);
                $text = 'Hello ' . $customer->name . ',<br/>Kindly find the attached quotation.<br/>Please let me know if you have any concerns.<br/>Regards,<br/>Marketing Misr';
                return view('quotation.compose', compact('quotation', 'customer', 'text'));
            }
        } else {
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $quotation = Quotation::findOrFail($id);
        $quotation->delete();

        \App\QuotationItem::where('quotation_id', $id)->delete();
        return json_encode(TRUE);
    }

    public function send(Request $request) {
        $file = Quotation::find($request->quotation_id)->file;
        $request->message = str_replace('<br>', PHP_EOL, $request->message);
        Mail::send([], [], function ($message) use ($request, $file) {
            $message->to($request->to)->from('info@marketingmisr.com')
                    ->subject($request->subject)->attach('storage/' . $file, [
                        'as' => $file,
                        'mime' => 'pdf',
                    ])
                    ->setBody($request->message, 'text/html');
        });
        return redirect('quotation')->with('status', 'Added And Sent Successfuly');
    }

    public function status(Request $request) {
        $quotation = Quotation::find($request->id);
        $quotation->status = 'Approved';
        $quotation->update();
        return json_encode(TRUE);
    }

}
