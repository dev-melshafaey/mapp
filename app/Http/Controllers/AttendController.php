<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\attends;

class AttendController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $data = attends::all();
        $users = \App\User::all();
        return view('attend.show', compact('data', 'users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $state = ($request->state == 'in') ? 'out' : 'in';

        $attend = new attends();
        $attend->user_id = auth()->id();
        $attend->state = $state;

        $attend->save();
        echo json_encode($state);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

    public function filter(Request $request) {
        $clauses = [];

        if ($request->from) {
            $clauses = array_merge($clauses, [['created_at', '>=', $request->from]]);
        }
        if ($request->to) {
            $clauses = array_merge($clauses, [['created_at', '<=', date('Y-m-d', strtotime($request->to . ' + 1 days'))]]);
        }

        if ($request->employee) {
            $clauses = array_merge($clauses, [['user_id', '=', $request->employee]]);
        }

        $data = attends::where($clauses)->get();
        $users = \App\User::all();
        return view('attend.show', compact('data', 'users'));
    }

}
