<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;

class PaymentController extends Controller {

    function __construct() {
        $this->middleware('permission:payement-list');
        $this->middleware('permission:payement-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:payement-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:payement-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $payments = Payment::orderBy('created_at', 'desc')->get();
        return view('payment.show', ['payments' => $payments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('payment.add-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $payment = new Payment();

        $payment->name = $request->name;

        if ($payment->save()) {
            return redirect('payment')->with('status', 'Added Successfuly');
        } else {
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $payment = Payment::findOrFail($id);
        return view('payment.add-edit', ['payment' => $payment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $payment = Payment::find($id);

        $payment->name = $request->name;

        if ($payment->update()) {
            return redirect('payment')->with('status', 'Updated Successfuly');
        } else {
            return back()->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $payment = Payment::findOrFail($id);
        if ($payment->delete()) {
            echo TRUE;
        } else {
            echo FALSE;
        }
    }

}
