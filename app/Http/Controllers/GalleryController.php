<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Gallery;
use Illuminate\Support\Facades\Input;
use Validator;

class GalleryController extends Controller {

    function __construct() {
        $this->middleware('permission:gallery-list');
        $this->middleware('permission:gallery-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:gallery-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:gallery-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $images = Gallery::limit(20)->get();
        return view('gallery.show', compact('images'));
    }

    public function loadDataAjax(Request $request) {
        $output = array();
        $skip = $request->id;
        $posts = Gallery::skip($skip)->take(20)->get();

        if (!$posts->isEmpty()) {
            foreach ($posts as $post) {

                $output[] = '<div><a style="max-width: 100%" href="' . asset('storage/uploads/gallery/images/' . $post->image) . '" class="highslide" onclick="return hs.expand(this)">
                            <img style="width: 100%" src="' . asset('storage/uploads/gallery/thumbnail/' . $post->image) . '" alt="Highslide JS"
                                 title="Click to enlarge" />
                        </a></div>';
            }
        }

        echo json_encode([$output, intval($skip) + 20]);
    }

    public function search(Request $request) {
        $output = array();
        $tag = $request->value;
//        $posts = Gallery::where('tag', 'like', '%' . $tag . '%')->get();
        $posts = Gallery::where('tag', $tag)->get();

        if (!$posts->isEmpty()) {
            foreach ($posts as $post) {

                $output [] = '<div><a style="max-width: 100%" href="' . asset('storage/uploads/gallery/images/' . $post->image) . '" class="highslide" onclick="return hs.expand(this)">
                            <img style="width: 100%" src="' . asset('storage/uploads/gallery/thumbnail/' . $post->image) . '" alt="Highslide JS"
                                 title="Click to enlarge" />
                        </a></div>';
            }
        }
        echo json_encode([$output]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('gallery.add');
    }

    public function post_upload(Request $request) {

        $image = $request->file('file');
        $imageName = time() . '.' . $image->getClientOriginalExtension();

        /**/
        $thumbnailImage = \Image::make($image);
        $thumbnailPath = public_path('/storage/uploads/gallery/thumbnail/');
        $thumbnailImage->resize(null, 250, function ($constraint) {
            $constraint->aspectRatio();
            $constraint->upsize();
        });

        $thumbnailImage->save($thumbnailPath . $imageName);
        /**/
        $upload_success = $image->move(public_path('storage/uploads/gallery/images'), $imageName);

        if ($upload_success && $request->tags != '') {
            $gallery = new Gallery();
            $gallery->image = $imageName;
            $gallery->user_id = auth()->id();
            $gallery->tag = $request->tags;

            $gallery->save();

            return response()->json($upload_success, 200);
        } else {
            return response()->json('error', 400);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $images = array();
        $files = $request->file('images');

        if ($files) {
            foreach ($files as $originalImage) {
                $thumbnailImage = \Image::make($originalImage);
                $name = time() . $originalImage->getClientOriginalExtension();
                $thumbnailPath = public_path('/storage/uploads/gallery/thumbnail/');
                \File::exists($thumbnailPath) or \File::makeDirectory($thumbnailPath, 0777, true, true);

                $originalPath = public_path('/storage/uploads/gallery/images/');
                \File::exists($originalPath) or \File::makeDirectory($originalPath, 0777, true, true);

                $thumbnailImage->save($originalPath . $name);

                $thumbnailImage->resize(null, 150, function ($constraint) {
                    $constraint->aspectRatio();
                    $constraint->upsize();
                });

                $thumbnailImage->save($thumbnailPath . $name);
                $images[] = $name;
            }
        }

        $save = Gallery::insert([
                    'image' => implode("|", $images),
                    'tag' => $request->tags,
                    'user_id' => auth()->id()
        ]);

        if ($save) {
            return redirect('gallery')->with('status', 'Added Successfuly');
        } else {
            return back()->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
