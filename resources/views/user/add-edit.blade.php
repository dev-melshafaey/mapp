@extends('index')
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Add new User</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Add new User</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-primary">

                    <div class="card-body">
                        <form method="POST" action="{{url('user/'.($user->id ?? '') )}}" enctype="multipart/form-data" >
                            @csrf
                            @if(isset($user->id))
                            @method('PUT')
                            @endif
                            <div class="form-body">
                                <h3 class="card-title m-t-15">User Info</h3>
                                <hr>                                                                                                                             
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <label>Name</label><span class="text-danger">*</span>
                                            <input value="{{ $user->name ?? old('name') }}" name="name" type="text" class="form-control">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email</label><span class="text-danger">*</span>
                                            <input value="{{ $user->email ?? old('email') }}" name="email" type="email" class="form-control">
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Password</label><span class="text-danger">*</span>
                                            <input name="password" type="password" class="form-control">
                                            @if ($errors->has('password'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--/span-->

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Confirm Password</label>
                                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">                                    
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Role</label><span class="text-danger">*</span>
                                            <select name="roles[]" class="multiple form-control">
                                                @foreach($roles as $role)
                                                @if(isset($userRole) && $userRole == $role)
                                                <option selected="" >{{$role}}</option>
                                                @else
                                                <option>{{$role}}</option>
                                                @endif                                                
                                                @endforeach
                                            </select>
                                            @if ($errors->has('roles'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('roles') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>                                                                     
                                </div>

                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                <!--<button type="button" class="btn btn-inverse">Cancel</button>-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
        <!-- Row -->

    </div>
    <!-- End Container fluid  -->

</div>
<!-- End Page wrapper  -->
@endsection