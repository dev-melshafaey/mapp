@extends('index')
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Add new Item</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Add new Item</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-primary">

                    <div class="card-body">
                        <form method="POST" action="{{url('item/'.($item->id ?? '') )}}" enctype="multipart/form-data" >
                            @csrf
                            @if(isset($item->id))
                            @method('PUT')
                            @endif
                            <div class="form-body">
                                <h3 class="card-title m-t-15">Item Info</h3>
                                <hr>                                                                                                                             
                                <div class="row">
                                    <div class="col-md-6 ">
                                        <div class="form-group">
                                            <label>Name</label><span class="text-danger">*</span>
                                            <input value="{{ $item->name ?? '' }}" name="name" type="text" class="form-control">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Unite Price</label>
                                            <input value="{{ $item->unit_price ?? 1 }}" name="unit_price" type="text" class="form-control">
                                            @if ($errors->has('unit_price'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('unit_price') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Description</label>
                                            <input value="{{ $item->description ?? '' }}" name="description" type="text" class="form-control">
                                            @if ($errors->has('description'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('description') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--/span-->
                                    
                                    <!--/span-->
                                </div>
                                <!--/row-->
                              
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                <!--<button type="button" class="btn btn-inverse">Cancel</button>-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
        <!-- Row -->

    </div>
    <!-- End Container fluid  -->

</div>
<!-- End Page wrapper  -->
@endsection