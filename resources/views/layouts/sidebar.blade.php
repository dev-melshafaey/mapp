<div class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">
                <li class="nav-devider"></li>
                <li class="nav-label">Home</li>
                @hasrole(['Admin'])
                <li> 
                    <a href="{{url('attend')}}" ><i class="fa fa-clock-o"></i><span class="hide-menu">Attendances </span></a>                    
                </li>
                @endhasrole
                @can('customer-list')
                <li> 
                    <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Customers <span class="label label-rouded @if(App\Customer::count() == 0) label-danger @else label-primary @endif pull-right">{{ App\Customer::count() }}</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('customer')}}">View Customers </a></li>
                        @can('customer-create')
                        <li><a href="{{url('customer/create')}}">Add new Customer</a></li>
                        @endcan
                    </ul>
                </li>
                @endcan
                @can('product-list')
                <li> 
                    <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-product-hunt"></i><span class="hide-menu">Product Items <span class="label label-rouded @if(App\ProductItem::count() == 0) label-danger @else label-primary @endif pull-right">{{ App\ProductItem::count() }}</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('item')}}">View Items </a></li>
                        @can('product-create')
                        <li><a href="{{url('item/create')}}">Add new item</a></li>
                        @endcan
                    </ul>
                </li>
                @endcan
                @can('payement-list')
                <li> 
                    <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-dollar"></i><span class="hide-menu">Payment Terms <span class="label label-rouded @if(App\ProductItem::count() == 0) label-danger @else label-primary @endif pull-right">{{ App\Payment::count() }}</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('payment')}}">View Terms </a></li>
                        @can('payement-create')
                        <li><a href="{{url('payment/create')}}">Add new term</a></li>
                        @endcan
                    </ul>
                </li>
                @endcan
                @can('quotation-list')
                <li> 
                    <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-empire"></i><span class="hide-menu">Quotations <span class="label label-rouded @if(App\Quotation::count() == 0) label-danger @else label-primary @endif pull-right">{{ App\Quotation::count() }}</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('quotation')}}">View Quotations </a></li>
                        @can('quotation-create')
                        <li><a href="{{url('quotation/create')}}">Add new quotation</a></li>
                        @endcan
                    </ul>
                </li>
                @endcan
                @can('invoice-list')
                <li> 
                    <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-paperclip"></i><span class="hide-menu">Invoices <span class="label label-rouded @if(App\Invoice::count() == 0) label-danger @else label-primary @endif pull-right">{{ App\Invoice::count() }}</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('invoice')}}">View Invoices </a></li>
                        @can('invoice-create')
                        <li><a href="{{url('invoice/create')}}">Add new invoice</a></li>
                        @endcan
                    </ul>
                </li>
                @endcan
                @can('gallery-list')
                <li> 
                    <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-image"></i><span class="hide-menu">Gallery <span class="label label-rouded @if(App\Gallery::count() == 0) label-danger @else label-primary @endif pull-right">{{ App\Gallery::count() }}</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('gallery')}}">View Gallery </a></li>
                        @can('gallery-create')
                        <li><a href="{{url('gallery/create')}}">Add new Image</a></li>
                        @endcan
                    </ul>
                </li>
                @endcan
                @can('user-list')
                <li> 
                    <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-users"></i><span class="hide-menu">Users <span class="label label-rouded @if(App\User::count() == 0) label-danger @else label-primary @endif pull-right">{{ App\User::count() }}</span></span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('user')}}">View Users </a></li>
                        @can('user-create')
                        <li><a href="{{url('user/create')}}">Add new User</a></li>
                        @endcan
                    </ul>
                </li>
                @endcan
                @can('role-list')
                <li> 
                    <a class="has-arrow  " href="#" aria-expanded="false"><i class="fa fa-key"></i><span class="hide-menu">Roles </span></a>
                    <ul aria-expanded="false" class="collapse">
                        <li><a href="{{url('roles')}}">View Roles </a></li>
                        @can('role-create')
                        <li><a href="{{url('roles/create')}}">Add new Role</a></li>
                        @endcan
                    </ul>
                </li>
                @endcan

               
            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
</div>