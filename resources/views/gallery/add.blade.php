@extends('index')
@section('content')

<style type="text/css">
    .dropzone {
        border:2px dashed #999999;
        border-radius: 10px;
    }
    .dropzone .dz-default.dz-message {
        height: 171px;
        background-size: 132px 132px;
        margin-top: -101.5px;
        background-position-x:center;

    }
    .dropzone .dz-default.dz-message span {
        display: block;
        margin-top: 145px;
        font-size: 20px;
        text-align: center;
    }
</style>
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Add new Image</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Add new Image</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-primary">

                    <div class="card-body">


                        <form action="{{ url('gallery/upload')}}" enctype="multipart/form-data" class="dropzone" id="fileupload" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Tags</label><span class="text-danger">*</span>
                                <input required class="tagsinput form-control" name="tags"  />
                                @if ($errors->has('tags'))
                                <span class="invalid-feedback">
                                    <strong>{{ $errors->first('tags') }}</strong>
                                </span>
                                @endif
                            </div>
                            <div class="fallback">
                                <input name="file" type="files" multiple accept="image/jpeg, image/png, image/jpg" />
                            </div>
                        </form>

                    </div>
                </div>
            </div>


        </div>
        <!-- Row -->

    </div>
    <!-- End Container fluid  -->

</div>
<!-- End Page wrapper  -->

@endsection