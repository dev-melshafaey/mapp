@extends('index')
@section('content')
<style>
    #grid[data-columns]::before {
        content: '3 .column.size-1of3';
    }

    /* These are the classes that are going to be applied: */
    .column { float: left; }
    .size-1of3 { width: 33.333%; }
</style>
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Image Gallery</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Image Gallery</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-md-12">              
                <div class="form-group">
                    <div class="input-group input-group-rounded">
                        <input onkeyup="getPhotos()" type="text" placeholder="Search Round" id="search" name="Search" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-primary btn-group-right" onclick="getPhotos()" type="button"><i class="ti-search"></i></button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <!--<div class="row grid" id="load-data" >-->                       
        <div id="grid" data-columns>                       
            @foreach($images as $image)
            <div>
              
                        <a style="max-width: 100%" href="{{asset('storage/uploads/gallery/images/'.$image->image)}}" class="highslide" onclick="return hs.expand(this)">
                            <img style="width: 100%" src="{{asset('storage/uploads/gallery/thumbnail/'.$image->image)}}" alt="Highslide JS"
                                 title="Click to enlarge" />
                        </a>              

                    
            </div>
            @endforeach  

        </div>
        <!-- Row -->

        <div id="remove-row" class="row text-center" style="position: fixed;bottom: 50px;right: 50px;">
            <button id="btn-more" data-id="20" class="btn btn-youtube text-white" > Load More </button>
        </div>
    </div>
    <!-- End Container fluid  -->

</div>
<!-- End Page wrapper  -->


@endsection