
@extends('index')
@section('content')
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Quotations</h3> 
        </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Quotations</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->


    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">

            <div class="col-12">
                @if (session('status'))
                <div class="alert alert-info">
                    {{ session('status') }}
                </div>
                @endif

                <div class="card">
                    <div class="card-body"> 
                        <a href="{{url('quotation/create')}}" class="btn btn-googleplus">Add New Item</a>
                        <div class="table-responsive m-t-40">

                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Title</th>
                                        <th>Customer</th>
                                        <th>Quotation Date</th>
                                        <th>Expiry Date</th>
                                        <th>Status</th>                                                                               
                                        <th>PDF</th>                                                                               
                                        <th>Actions</th>

                                    </tr>
                                </thead>                                
                                <tbody>
                                    @foreach($quotations as $quotation)
                                    <tr>
                                        <td>{{$quotation->title}}</td>
                                        <td>{{$quotation->customer->name}}</td>
                                        <td>{{$quotation->quotation_date}}</td>
                                        <td>{{$quotation->expire_date}}</td>

                                        <td><a href="javascript:void(0)" class="btn btn-xs @if($quotation->status == 'Approved') btn-success @else btn-danger @endif">{{$quotation->status}}</a></td>                                      
                                        <td><a target="_blank" href="{{asset('storage/'.$quotation->file)}}"><i class="fa fa-file-pdf-o text-danger btn btn-lg"></i></a></td>                                      
                                        <td>
                                            <a title="edit" href="{{url('quotation/'.$quotation->id.'/edit')}}" class="btn btn-info btn-xs"><i class="fa fa-pencil"></i></a>
                                            <button onclick="deleteItem({{$quotation->id}}, 'quotation', this)" title="delete" class="btn btn-danger btn-xs"><i class="fa fa-remove"></i></button>
                                            @if($quotation->status != 'Approved')
                                            <button onclick="updateStatus('quotation',{{$quotation->id}})" title="approve" class="btn btn-primary btn-xs"><i class="fa fa-check"></i></button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th>Title</th>
                                        <th>Customer</th>
                                        <th>Quotation Date</th>
                                        <th>Expiry Date</th>
                                        <th>Payment Term</th>                                                                               
                                        <th>Notes</th>                                                                               

                                    </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>                                                             
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->

</div>
@endsection