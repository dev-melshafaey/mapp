<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Favicon icon -->
        <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/favicon.png')}}">
        <title>System Management MAPP</title>

        <link rel="stylesheet" href="{{asset('css/lib/html5-editor/bootstrap-wysihtml5.css')}}" />
        <link href="{{asset('css/lib/dropzone/dropzone.css')}}" rel="stylesheet">
        <link href="{{asset('css/lib/sweetalert/sweetalert.css')}}" rel="stylesheet">

        <!-- Bootstrap Core CSS -->
        <link href="{{asset('css/lib/bootstrap/bootstrap.min.css')}}" rel="stylesheet">
        <!-- Custom CSS -->

        <link href="{{asset('css/lib/calendar2/semantic.ui.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/lib/calendar2/pignose.calendar.min.css')}}" rel="stylesheet">
        <link href="{{asset('css/lib/owl.carousel.min.css')}}" rel="stylesheet" />
        <link href="{{asset('css/lib/owl.theme.default.min.css')}}" rel="stylesheet" />
        <link href="{{asset('css/helper.css')}}" rel="stylesheet">
        <link href="{{asset('css/style.css')}}" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:** -->
        <!--[if lt IE 9]>
    <script src="https:**oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https:**oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->


    </head>

    <body class="fix-header fix-sidebar">
        <!-- Preloader - style you can find in spinners.css -->
        <div class="preloader">
            <svg class="circular" viewBox="25 25 50 50">
            <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
        </div>
        <!-- Main wrapper  -->
        <div id="main-wrapper">
            @include('layouts.ui')
            @auth
            <!-- header header  -->
            @include('layouts.header')
            <!-- End header header -->
            <!-- Left Sidebar  -->
            @include('layouts.sidebar')               
            @endauth

            <!-- End Left Sidebar  -->
            <!-- Page wrapper  -->
            @yield('content')
            <!-- End Page wrapper  -->
        </div>
        <!-- End Wrapper -->
        <!-- All Jquery -->
        <script src="{{asset('js/lib/jquery/jquery.min.js')}}"></script>
        <!-- Bootstrap tether Core JavaScript -->
        <script src="{{asset('js/lib/bootstrap/js/popper.min.js')}}"></script>
        <script src="{{asset('js/lib/bootstrap/js/bootstrap.min.js')}}"></script>
        <!-- slimscrollbar scrollbar JavaScript -->
        <script src="{{asset('js/jquery.slimscroll.js')}}"></script>
        <!--Menu sidebar -->
        <script src="{{asset('js/sidebarmenu.js')}}"></script>
        <!--stickey kit -->
        <script src="{{asset('js/lib/sticky-kit-master/dist/sticky-kit.min.js')}}"></script>
        <!--Custom JavaScript -->
        <script src="{{asset('js/lib/html5-editor/wysihtml5-0.3.0.js')}}"></script>
        <script src="{{asset('js/lib/html5-editor/bootstrap-wysihtml5.js')}}"></script>
        <script src="{{asset('js/lib/html5-editor/wysihtml5-init.js')}}"></script>

        <!-- Amchart -->
        <script src="{{asset('js/lib/morris-chart/raphael-min.js')}}"></script>
        <script src="{{asset('js/lib/morris-chart/morris.js')}}"></script>
        <script src="{{asset('js/lib/morris-chart/dashboard1-init.js')}}"></script>


        <script src="{{asset('js/lib/calendar-2/moment.latest.min.js')}}"></script>
        <!-- scripit init-->
        <script src="{{asset('js/lib/calendar-2/semantic.ui.min.js')}}"></script>
        <!-- scripit init-->
        <script src="{{asset('js/lib/calendar-2/prism.min.js')}}"></script>
        <!-- scripit init-->
        <script src="{{asset('js/lib/calendar-2/pignose.calendar.min.js')}}"></script>
        <!-- scripit init-->
        <script src="{{asset('js/lib/calendar-2/pignose.init.js')}}"></script>

        <script src="{{asset('js/lib/owl-carousel/owl.carousel.min.js')}}"></script>
        <script src="{{asset('js/lib/owl-carousel/owl.carousel-init.js')}}"></script>
        <script src="{{asset('js/scripts.js')}}"></script>

        <script src="{{asset('js/lib/sweetalert/sweetalert.min.js')}}"></script>        

        <script src="{{asset('js/custom.min.js')}}"></script>
        <script src="{{asset('js/lib/portlets/portlets.js')}}"></script>

        <script src="{{asset('js/lib/datatables/datatables.min.js')}}"></script>
        <script src="{{asset('js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js')}}"></script>
        <script src="{{asset('js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js')}}"></script>
        <script src="{{asset('js/lib/datatables/cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js')}}"></script>
        <script src="{{asset('js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js')}}"></script>
        <script src="{{asset('js/lib/datatables/cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js')}}"></script>
        <script src="{{asset('js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js')}}"></script>
        <script src="{{asset('js/lib/datatables/cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js')}}"></script>
        <script src="{{asset('js/lib/datatables/datatables-init.js')}}"></script>


        <script type="text/javascript" src="{{URL::asset('js/jquery.tagsinput-revisited.js')}}"></script>
        <link rel="stylesheet" type="text/css" href="{{ URL::asset('css/jquery.tagsinput-revisited.css') }}">

        <script type="text/javascript" src="{{asset('highslide/highslide-with-gallery.js')}}"></script>
        <link rel="stylesheet" type="text/css" href="{{asset('highslide/highslide.css')}}" />

<!--        <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-infinitescroll/2.0b2.120519/jquery.infinitescroll.min.js"></script>
        <script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
        <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>-->

        <script src="{{asset('js/salvattore.min.js')}}"></script>

        <script src="{{asset('js/lib/dropzone/dropzone.js')}}"></script>

        <script type="text/javascript">
Dropzone.options.fileupload = {
    accept: function (file, done) {
        if (file.type != "application/vnd.ms-excel" && file.type != "image/jpeg, image/png, image/jpg") {
            done("Error! Files of this type are not accepted");
        } else {
            done();
        }
    }
}

Dropzone.options.fileupload = {
    acceptedFiles: "image/jpeg, image/png, image/jpg"
}

if (typeof Dropzone != 'undefined') {
    Dropzone.autoDiscover = false;
}

;
(function ($, window, undefined) {
    "use strict";

    $(document).ready(function () {

        if (typeof Dropzone != 'undefined') {
            if ($("#fileupload").length) {
                var dz = new Dropzone("#fileupload"),
                        dze_info = $("#dze_info"),
                        status = {
                            uploaded: 0,
                            errors: 0
                        };
                var $f = $('<tr><td class="name"></td><td class="size"></td><td class="type"></td><td class="status"></td></tr>');
                dz.on("success", function (file, responseText) {

                    var _$f = $f.clone();

                    _$f.addClass('success');

                    _$f.find('.name').html(file.name);
                    if (file.size < 1024) {
                        _$f.find('.size').html(parseInt(file.size) + ' KB');
                    } else {
                        _$f.find('.size').html(parseInt(file.size / 1024, 10) + ' KB');
                    }
                    _$f.find('.type').html(file.type);
                    _$f.find('.status').html('Uploaded <i class="entypo-check"></i>');

                    dze_info.find('tbody').append(_$f);

                    status.uploaded++;

                    dze_info.find('tfoot td').html('<span class="label label-success">' + status.uploaded + ' uploaded</span> <span class="label label-danger">' + status.errors + ' not uploaded</span>');

                    //swal('Your File Uploaded Successfully!!', '', 'success');

                })
                        .on('error', function (file) {

                            var _$f = $f.clone();

                            dze_info.removeClass('hidden');

                            _$f.addClass('danger');

                            _$f.find('.name').html(file.name);
                            _$f.find('.size').html(parseInt(file.size / 1024, 10) + ' KB');
                            _$f.find('.type').html(file.type);
                            _$f.find('.status').html('Uploaded <i class="entypo-cancel"></i>');

                            dze_info.find('tbody').append(_$f);

                            status.errors++;

                            dze_info.find('tfoot td').html('<span class="label label-success">' + status.uploaded + ' uploaded</span> <span class="label label-danger">' + status.errors + ' not uploaded</span>');

                            swal('Your File Uploaded Not Successfully!!', '', 'error');
                        });
            }
        }
    });
})(jQuery, window);
        </script>

        <script type="text/javascript">

//            var $grid = $('.grid').masonry({
//                itemSelector: '.grid-item'
//            });
//
//            $grid.imagesLoaded().progress(function () {
//                $grid.masonry('layout');
//            });


            $('#btn-more').on('click', function () {
                var id = $('#btn-more').attr('data-id');
                $("#btn-more").html("Loading....");
                $.ajax({
                    url: '{{ url("gallery/loaddata") }}',
                    method: "POST",
                    data: {id: id, _token: "{{csrf_token()}}"},
                    dataType: "text",
                    success: function (data)
                    {
                        data = JSON.parse(data);

                        if (data[0] != '') {
                            var grid = document.querySelector('#grid');
                            for (i = 0; i < data[0].length; i++) {
                                var item = document.createElement('article');
                                salvattore.appendElements(grid, [item]);
                                item.outerHTML = data[0][i];
                            }
                            $('#btn-more').attr('data-id', data[1]);
//                            var $newItems = $(data[0]);
//                            $('#load-data').append($newItems);
//                            $('#load-data').masonry('appended', $newItems).masonry('layout');
                        } else {
                            $('#btn-more').html("No Data");
                        }
                    }
                });
            });

            hs.graphicsDir = '../highslide/graphics/';
            hs.align = 'center';
            hs.transitions = ['expand', 'crossfade'];
            hs.outlineType = 'glossy-dark';
            hs.wrapperClassName = 'dark';
            hs.fadeInOut = true;
//hs.dimmingOpacity = 0.75;

// Add the controlbar
            if (hs.addSlideshow)
                hs.addSlideshow({
                    //slideshowGroup: 'group1',
                    interval: 5000,
                    repeat: false,
                    useControls: true,
                    fixedControls: 'fit',
                    overlayOptions: {
                        opacity: .6,
                        position: 'bottom center',
                        hideOnMouseOut: true
                    }
                });

            $('.tagsinput').tagsInput({
                'delimiter': ' '
            });

        </script>
    </body>

</html>