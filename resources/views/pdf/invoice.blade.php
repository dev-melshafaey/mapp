<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <!--<link href="https://fonts.googleapis.com/css?family=Open+Sans|Source+Sans+Pro" rel="stylesheet">--> 
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <img height="100" src="https://www.marketingmisr.com/wp-content/uploads/2016/06/logoweb-new.png" alt="" />

                </div>
                <div class="col-md-12">
                    <div style="width: 70%;float: left" >
                        <p style="font-size: 14px"> Marketing Misr </p><p> 7/8 Lasilky Street, Maadi, Cairo - Egypt</p>
                    </div>
                    <div style="width: 30%;float: right;" >
                        <p style="font-size: 14px"> Invoice: {{$request->title}} </p>                        
                        <p> Issued date: {{ \Carbon\Carbon::parse($request->order_date)->format('M d, Y')}} <br>
                            Expire date: @if($request->expiry_date) {{ \Carbon\Carbon::parse($request->expiry_date)->format('M d, Y')}} @endif </p>
                    </div>
                </div>
            </div>
            <br/>
            <br/>
            <div class="row">
                <div class="col-md-12">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Product</th>
                                <th>Description</th>
                                <th >Qty</th>
                                <th >Price</th>
                                <th >Discount</th>
                                <th >Tax</th>
                                <th >Net</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($request->items as $i => $value) {
                            <tr>
                                <td>{{explode('_',$value)[1]}}</td>
                                <td>{{$request->description[$i]}}</td>
                                <td>{{$request->qtys[$i]}}</td>
                                <td>{{$request->unit_prices[$i]}}</td>
                                <td>{{$request->discounts[$i]}}</td>
                                <td>{{$request->taxes[$i]}}</td>
                                <td>{{$request->unit_prices[$i]*$request->qtys[$i]}}</td>
                                <td>{{$request->unit_prices[$i]*$request->qtys[$i] + ($request->taxes[$i]/100)*$request->unit_prices[$i]*$request->qtys[$i] - ($request->discounts[$i]/100)*$request->unit_prices[$i]*$request->qtys[$i] }}</td>

                            </tr>

                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="6"></th>
                                <th>Total</th>
                                <th>{{$request->grand_total}} LE</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>

            <div class="footer text-center" style="position: fixed;bottom: 30px;left: 50%;transform: translate(-50%, 0);">
                <p style="font-size: 12px;">
                    Land Line: 02-25174231 Mobile: 01090028002 - 01095501055 - 01022773003 Email: info@marketingmisr.com 
                    <br>Website: www.marketingmisr.com
                </p>
            </div>
        </div>
    </body>
</html>





