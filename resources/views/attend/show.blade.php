

@extends('index')
@section('content')
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Attendance</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Attendance</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">

            <div class="col-12">

                <div class="card">
                    <div class="card-body">    
                        <div class="row">
                            <div class="col-md-12">

                                <form method="post" action="{{url('attend/filter')}}" class="form-inline">
                                    @csrf
                                    <div class="form-group">
                                        <label for="">From: </label>&nbsp;
                                        <input type="date" class="form-control" name="from">
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <div class="form-group">
                                        <label for="">To: </label>&nbsp;
                                        <input type="date" class="form-control" name="to">
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <div class="form-group">
                                        <label for="">User: </label>&nbsp;
                                        <select name="employee" class="form-control">
                                            @foreach($users as $user)
                                            <option value="{{$user->id}}">{{$user->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    &nbsp;&nbsp;&nbsp;&nbsp;
                                    <button type="submit" class="btn btn-instagram">Filter</button>
                                </form>

                            </div>
                        </div>
                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>State</th>
                                        <th>Time</th>                                      
                                    </tr>
                                </thead>                                
                                <tbody>
                                    @foreach($data as $attend)
                                    <tr>
                                        <td><strong>{{$attend->user->name}}</strong></td>
                                        <td><a href="" class="btn @if($attend->state == 'in') btn-info @else btn-danger @endif">{{$attend->state}}</a></td>
                                        <td><strong>{{$attend->created_at}}</strong></td>                                       
                                    </tr>
                                    @endforeach
                                </tbody>

                            </table>
                        </div>
                    </div>
                </div>                                                             
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->

</div>
@endsection