@extends('index')
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Add new Customer</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Add new Customer</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-primary">

                    <div class="card-body">
                        <form method="POST" action="{{url('customer/'.($customer->id ?? '') )}}" enctype="multipart/form-data" >
                            @csrf
                            @if(isset($customer->id))
                            @method('PUT')
                            @endif
                            <div class="form-body">
                                <h3 class="card-title m-t-15">Customer Info</h3>
                                <hr>                                                                                                                             
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label>Name</label><span class="text-danger">*</span>
                                            <input value="{{ $customer->name ?? '' }}" name="name" type="text" class="form-control">
                                            @if ($errors->has('name'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Email</label><span class="text-danger">*</span>
                                            <input value="{{ $customer->email ?? '' }}" name="email" type="email" class="form-control">
                                            @if ($errors->has('email'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--/span-->
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Phone</label><span class="text-danger">*</span>
                                            <input value="{{ $customer->phone ?? '' }}" name="phone" type="text" class="form-control">
                                            @if ($errors->has('phone'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('phone') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <!--/span-->
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Address</label><span class="text-danger">*</span>
                                            <textarea rows="5" class="form-control textarea" name="address">{{ $customer->address ?? '' }}</textarea>
                                            @if ($errors->has('address'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Web site</label>
                                            <input value="{{ $customer->website ?? '' }}" name="website" type="text" class="form-control">
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label>Logo</label>
                                            <input name="logo" type="file" class="form-control">
                                            @if(isset($customer->logo) && $customer->logo)
                                            <img height="64" width="128" src="{{asset('storage/'.$customer->logo)}}" />
                                            @endif
                                            @if ($errors->has('logo'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('logo') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div class="form-actions">
                                <button type="submit" class="btn btn-success"> <i class="fa fa-check"></i> Save</button>
                                <!--<button type="button" class="btn btn-inverse">Cancel</button>-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
        <!-- Row -->

    </div>
    <!-- End Container fluid  -->

</div>
<!-- End Page wrapper  -->
@endsection