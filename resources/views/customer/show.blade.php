

@extends('index')
@section('content')
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Customers</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Customers</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">

            <div class="col-12">
                @if (session('status'))
                <div class="alert alert-info">
                    {{ session('status') }}
                </div>
                @endif
                <div class="card">
                    <div class="card-body">
                        @can('customer-create')
                        <a href="{{url('customer/create')}}" class="btn btn-googleplus">Add New Item</a>
                        @endcan
                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Website</th>
                                        <th>Address</th>
                                        <th>logo</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>                                
                                <tbody>
                                    @foreach($customers as $customer)
                                    <tr>
                                        <td>{{$customer->name}}</td>
                                        <td>{{$customer->email}}</td>
                                        <td>{{$customer->phone}}</td>
                                        <td><a target="_blank" href='http://{{$customer->website}}'>{{$customer->website}}</a></td>
                                        <td>{{$customer->address}}</td>
                                        <td>
                                            @if($customer->logo)
                                            <img height="32" width="64" src="{{asset('storage/'.$customer->logo)}}" />
                                            @endif
                                        </td>
                                        <td>
                                            @can('customer-edit')
                                            <a title="edit" href="{{url('customer/'.$customer->id.'/edit')}}" class="btn btn-info"><i class="fa fa-pencil"></i></a>
                                            @endcan
                                            @can('customer-delete')
                                            <button onclick="deleteItem({{$customer->id}}, 'customer', this)" title="delete" class="btn btn-danger"><i class="fa fa-remove"></i></button>
                                            @endcan
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                                                             
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->

</div>
@endsection