

@extends('index')
@section('content')
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Payment Terms</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Payment Terms</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">

            <div class="col-md-12">
                @if (session('status'))
                <div class="alert alert-info">
                    {{ session('status') }}
                </div>
                @endif
                <div class="card">
                    <div class="card-body"> 
                        <a href="{{url('payment/create')}}" class="btn btn-googleplus">Add New Item</a>
                        <div class="table-responsive m-t-40">
                            <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                       
                                        <th>Actions</th>
                                       
                                    </tr>
                                </thead>                                
                                <tbody>
                                    @foreach($payments as $payment)
                                    <tr>
                                        <td>{{$payment->name}}</td>
                                       
                                        <td>
                                            <a title="edit" href="{{url('payment/'.$payment->id.'/edit')}}" class="btn btn-info"><i class="fa fa-pencil"></i></a>
                                            <button onclick="deleteItem({{$payment->id}}, 'payment', this)" title="delete" class="btn btn-danger"><i class="fa fa-remove"></i></button>


                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>                                                             
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->

</div>
@endsection