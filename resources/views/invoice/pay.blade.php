@extends('index')
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Invoice Payments</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Invoice Payments</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-primary">

                    <div class="card-body">
                        <h3 class="card-title m-t-15">Payment History</h3>
                        <hr>
                        <table id="example23" class="display nowrap table table-hover table-striped table-bordered" cellspacing="0" width="100%">
                            <thead>
                                <tr>
                                    <th>Amount</th>
                                    <th>Date</th>
                                    <th>Document</th>
                                    <th>Notes</th>
                                    <th>Created date</th>                                                   
                                </tr>
                            </thead>                                
                            <tbody>
                                @php $all = 0; @endphp
                                @foreach($payments as $row)
                                @php $all += $row->amount @endphp
                                <tr>
                                    <td>{{$row->amount}}</td>
                                    <td>{{$row->payment_date}}</td>
                                    <td>
                                        @if($row->file)
                                        <a target="_blank" href="{{asset('storage/'.$row->file)}}"><i class="fa fa-file-pdf-o text-danger btn btn-lg"></i></a>
                                        @endif
                                    </td>
                                    <td>{{$row->notes}}</td>                                                   
                                    <td>{{$row->created_at}}</td>                                                   
                                </tr>
                                @endforeach                               
                            </tbody>
                        </table>

                        <br>
                        <hr>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group ">
                                    <label class="h6 text-inverse">Paid Amount:</label>
                                    <a href="javascript:void(0)" class="btn btn-success btn-sm">{{$all}}</a>                                    
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group text-center">
                                    <label class="h6 text-inverse">Invoice value:</label>
                                    <a href="javascript:void(0)" class="btn btn-info btn-sm">{{$invoice->total_value}}</a>                                    
                                </div>
                            </div>
                             <div class="col-md-4">
                                <div class="form-group text-right">
                                    <label class="h6 text-inverse">Remaining:</label>
                                    <a href="javascript:void(0)" class="btn btn-danger btn-sm">{{$invoice->total_value - $all }}</a>                                    
                                </div>
                            </div>
                        </div>

                        <form method="POST" class="quotation_form" action="{{url('invoice/'.$invoice_id.'/pay')}}" enctype="multipart/form-data" >
                            @csrf

                            <div class="form-body">
                                <h3 class="card-title m-t-15">Invoice Info</h3>
                                <hr>  


                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Payment Amount</label><span class="text-danger">*</span>
                                            <input required="" min='1' step='.1' oninput='validity.valid||(value=\"1\");' value='1' name='amount' class='form-control' type='number' />
                                            @if ($errors->has('amount'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('amount') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label>Payment date</label><span class="text-danger">*</span>
                                            <input required="" type="date" name="date" class="form-control">
                                            @if ($errors->has('date'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('date') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>  

                                    <div class="col-md-4 ">
                                        <div class="form-group">
                                            <label>Payment document</label>
                                            <input type="file" name="document" />                                            
                                        </div>
                                    </div>

                                </div>                               

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Notes</label>
                                            <textarea name="notes" rows="2" class="form-control"></textarea>                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <div class="col-sm-offset-2 col-sm-10">
                                                <div class="checkbox">
                                                    <label>
                                                        <input name="done" type="checkbox"> Full Paid
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-actions">
                                <button type="submit" name="submit" value="save" class="btn btn-success pull-right"> <i class="fa fa-check"></i> Save</button>                                
                                <a href="{{url('invoice')}}" class="btn btn-inverse"> Cancel</a>                                
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
        <!-- Row -->

    </div>
    <!-- End Container fluid  -->

</div>
<!-- End Page wrapper  -->

@endsection