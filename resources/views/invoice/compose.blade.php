@extends('index')
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Send Invoice</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Send Invoice</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-content">

                            <div class="">                            
                                <div class="mt-4">

                                    <form method="post" action="{{url('invoice/send')}}">
                                        @csrf
                                        <div class="form-group">
                                            <label>To</label>
                                            <input type="email" required="" value="{{$customer->email}}" name="to" class="form-control" placeholder="To">
                                        </div>

                                        <div class="form-group">
                                            <label>Subject</label>
                                            <input type="text" required="" class="form-control" name="subject" value="{{$invoice->title}}" placeholder="Subject">
                                        </div>
                                        <div class="form-group">
                                            <label>Message</label>
                                            <textarea name="message" class="textarea_editor form-control" rows="15" placeholder="" style="height:450px">{!! $text !!}</textarea>
                                            <!--<textarea name="message" required="" rows="8" cols="80" class="form-control" style="height:300px">{!! $text !!}</textarea>-->                                           
                                        </div>
                                        <div class="form-group">
                                            <label>Attachment:</label>
                                            <a target="_blank" href="{{asset('storage/'.$invoice->file)}}"><i class="fa fa-file-pdf-o text-danger btn btn-lg"></i></a>
                                            <input type="hidden" value="{{$invoice->id}}" name="invoice_id" />
                                        </div>

                                        <div class="form-group m-b-0">
                                            <div class="text-right">                                               
                                                <button type="button" onclick="location.href = '/invoice'" class="btn btn-danger waves-effect waves-light"><i class="fa fa-remove"></i> <span>Cancel</span> </button>
                                                <button type="submit" class="btn btn-success waves-effect waves-light"><i class="fa fa-send"></i> <span>Send</span>  </button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <!-- end card-->

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End PAge Content -->
    </div>
    <!-- End Container fluid  -->   
</div>
<!-- End Page wrapper  -->
@endsection