@extends('index')
@section('content')
<!-- Page wrapper  -->
<div class="page-wrapper">
    <!-- Bread crumb -->
    <div class="row page-titles">
        <div class="col-md-5 align-self-center">
            <h3 class="text-primary">Add new Invoice</h3> </div>
        <div class="col-md-7 align-self-center">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="javascript:void(0)">Home</a></li>
                <li class="breadcrumb-item active">Add new Invoice</li>
            </ol>
        </div>
    </div>
    <!-- End Bread crumb -->
    <!-- Container fluid  -->
    <div class="container-fluid">
        <!-- Start Page Content -->
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-outline-primary">

                    <div class="card-body">
                        <form method="POST" class="quotation_form" action="{{url('invoice/'.($invoice->id ?? '') )}}" enctype="multipart/form-data" >
                            @csrf
                            @if(isset($invoice->id))
                            @method('PUT')
                            @endif
                            <div class="form-body">
                                <h3 class="card-title m-t-15">Invoice Info</h3>
                                <hr>  
                                <div class="row">
                                    <div class="col-md-12 ">
                                        <div class="form-group">
                                            <label>Invoice title</label><span class="text-danger">*</span>
                                            <input required="" value="{{$title ?? $invoice->title}}" type="text" name="title" class="form-control">
                                            @if ($errors->has('title'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('title') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4 ">
                                        <div class="form-group">
                                            <label>Customer</label><span class="text-danger">*</span>
                                            <select required="" name="customer_id" onchange="getinfo(this)" class="form-control">
                                                <option value="0">---</option>
                                                @foreach($customers as $customer)
                                                @if(isset($invoice) && $invoice->customer_id == $customer->id)
                                                <option selected="" value="{{$customer->id}}">{{$customer->name}}</option>
                                                @else
                                                <option value="{{$customer->id}}">{{$customer->name}}</option>
                                                @endif
                                                @endforeach
                                            </select>
                                            @if ($errors->has('customer_id'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('customer_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4 ">
                                        <div class="form-group">
                                            <label>Order Date</label><span class="text-danger">*</span>
                                            <input required="" value="{{$invoice->invoice_date ?? ''}}" type="date" name="order_date" class="form-control" placeholder="dd/mm/yyyy">
                                            @if ($errors->has('order_date'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('order_date') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4 ">
                                        <div class="form-group">
                                            <label>Expiry Date</label>
                                            <input type="date" value="{{$invoice->expire_date ?? ''}}" name="expiry_date" class="form-control" placeholder="dd/mm/yyyy">
                                            @if ($errors->has('expiry_date'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('expiry_date') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-4 ">
                                        <div class="form-group">
                                            <label>Invoice Address</label><span class="text-danger">*</span>
                                            <input required="" value="{{$invoice->invoice_address ?? ''}}" type="text" class="form-control" id="invoice_address" name="invoice_address" >
                                            @if ($errors->has('invoice_address'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('invoice_address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4 ">
                                        <div class="form-group">
                                            <label>Delivery Address</label><span class="text-danger">*</span>
                                            <input required=""  value="{{$invoice->delivery_address ?? ''}}" type="text" class="form-control" id="delivery_address" name="delivery_address" >
                                            @if ($errors->has('delivery_address'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('delivery_address') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="col-md-4 ">
                                        <div class="form-group">
                                            <label>Payment Term</label><span class="text-danger">*</span>
                                            <select required="" name="payment_id" class="form-control">
                                                @foreach($payments as $payment)
                                                @if(isset($invoice) && $payment->id == $invoice->payment_id)
                                                <option selected="" value="{{$payment->id}}">{{$payment->name}}</option>
                                                @else
                                                <option value="{{$payment->id}}">{{$payment->name}}</option>
                                                @endif

                                                @endforeach
                                            </select>
                                            @if ($errors->has('payment_id'))
                                            <span class="invalid-feedback">
                                                <strong>{{ $errors->first('payment_id') }}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <table class="table table-hover table-bordered list_products">
                                            <thead> 
                                                <tr>
                                                    <th style="width: 12%"> Product </th>
                                                    <th> Description </th>
                                                    <th> Order Qty </th>
                                                    <th> Unit Price </th>
                                                    <th> Discount(%) </th>
                                                    <th> Taxes(%)</th>
                                                    <th> Net </th>
                                                    <th> Subtotal </th>
                                                    <th> </th>
                                                </tr>
                                            </thead>
                                            <tbody id="quote_items_body">
                                                @if(isset($invoice))
                                                @foreach($invoice_items as $item)
                                                <tr>
                                                    <td>
                                                        <select name='items[]' class='form-control'>
                                                            @foreach($prod_items as $pro)
                                                            @if($pro->id == $item->item_id)
                                                            <option selected="" value="{{$pro->id.'_'.$pro->name}}">{{$pro->name}}</option>
                                                            @else
                                                            <option value="{{$pro->id}}">{{$pro->name}}</option>
                                                            @endif
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td><textarea name='description[]' value='{{$item->description}}' class='form-control'></textarea></td>
                                                    <td><input id='qty_{{$invoice->id.'_'.$item->id}}' value='{{$item->qty}}' min='1' step='1' oninput='validity.valid||(value=\"1\");' value='1' name='qtys[]' onchange='calcaulate("{{$invoice->id.'_'.$item->id}}")' class='form-control' type='number' /></td>
                                                    <td><input id='unt_{{$invoice->id.'_'.$item->id}}' value='{{$item->unit_price}}' min='0' step='.5' oninput='validity.valid||(value=\"0\");' value='1' name='unit_prices[]' onchange='calcaulate("{{$invoice->id.'_'.$item->id}}")' class='form-control' type='number' /></td>
                                                    <td><input id='dis_{{$invoice->id.'_'.$item->id}}' value='{{$item->discount}}' min='0' step='.5' max='100' oninput='validity.valid||(value=\"0\");' value='0' name='discounts[]' onchange='calcaulate("{{$invoice->id.'_'.$item->id}}")' class='form-control' type='number' /></td>
                                                    <td><input id='tax_{{$invoice->id.'_'.$item->id}}' value='{{$item->tax}}' min='0' step='.5' max='100' oninput='validity.valid||(value=\"0\");' value='0' name='taxes[]' onchange='calcaulate("{{$invoice->id.'_'.$item->id}}")' class='form-control' type='number' /></td>
                                                    <td><span class='text-danger net' id='net_{{$invoice->id.'_'.$item->id}}'>{{$item->qty * $item->unit_price}}</span></td>
                                                    <td><span class='text-success tot' id='tot_{{$invoice->id.'_'.$item->id}}'>{{$item->qty * $item->unit_price - (($item->discount/100)*($item->qty * $item->unit_price)) + (($item->tax/100)*($item->qty * $item->unit_price))}}</span></td>
                                                    <td><i onclick='$(this).parents("tr").remove(); calculateGrand()' class='fa fa-remove text-danger'></i></td>
                                                </tr>
                                                @endforeach
                                                @endif
                                                <tr class="add_more_row">
                                                    <td colspan="6" style="text-align: left">
                                                        <button type="button" onclick="addItemRow()" class="btn btn-outline-info btn-sm">Add new</button>                                                        
                                                    </td>
                                                    <td><span class="text-danger" id="net_totlal"></span></td>
                                                    <td><input readonly="" name="grand_total" class="text-success form-control" id="grand_total" /></td>
                                                    <td></td>
                                                </tr>
                                            </tbody>                                           
                                        </table>
                                    </div>
                                </div>
                                <br/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label>Notes</label>
                                            <textarea name="notes" rows="5" class="form-control">{{$invoice->notes ?? ''}}</textarea>                                            
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="form-actions">
                                <button type="submit" name="submit" value="save" class="btn btn-success pull-right"> <i class="fa fa-check"></i> Save</button>
                                <button type="submit" name="submit" value="validate" class="btn btn-info "> <i class="fa fa-check-circle"></i> Validate</button>
                                <button style="display: none" type="button" class="btn btn-inverse"><i class="fa fa-registered"></i> Register</button>
                                <button style="display: none" type="button" class="btn btn-primary"><i class="fa fa-send"></i> Send</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


        </div>
        <!-- Row -->

    </div>
    <!-- End Container fluid  -->

</div>
<!-- End Page wrapper  -->
<script>
    window.onload = function(e){
    calculateGrand();
    };
</script>
@endsection