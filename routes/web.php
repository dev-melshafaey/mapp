<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Auth::routes();

Route::group(['middleware' => ['auth']], function () {
    
    Route::get('/', 'HomeController@index');
    Route::get('home', 'HomeController@index');

    Route::resource('customer', 'CustomerController');
    Route::resource('item', 'ItemController');
    Route::resource('payment', 'PaymentController');
    Route::resource('quotation', 'QuotationController');

    Route::resource('invoice', 'InvoiceController');
    Route::post('invoice/send', 'InvoiceController@send');
    Route::post('invoice/status', 'InvoiceController@status');
    Route::get('invoice/{id}/pay', 'InvoiceController@pay');
    Route::post('invoice/{id}/pay', 'InvoiceController@add_pay');

    Route::post('quotation/send', 'QuotationController@send');
    Route::post('quotation/status', 'QuotationController@status');

    Route::resource('gallery', 'GalleryController');
    Route::post('gallery/loaddata', 'GalleryController@loadDataAjax');
    Route::post('gallery/search', 'GalleryController@search');
    Route::post('gallery/upload', 'GalleryController@post_upload');

    Route::resource('attend', 'AttendController');
    Route::post('attend/filter', 'AttendController@filter');

    Route::resource('user', 'UserController');
    Route::resource('roles', 'RoleController');
});


